//
//  FotomultasCell.h
//  Fotomultas
//
//  Created by Marlon David Ruiz Arroyave on 4/09/15.
//  Copyright (c) 2015 Marlon David Ruiz Arroyave. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface MultaCell : UITableViewCell

@property (strong, nonatomic) IBOutlet UILabel *infraccionLabel;
@property (strong, nonatomic) IBOutlet UILabel *fechaLabel;
@property (strong, nonatomic) IBOutlet UIImageView *imageMulta;

@end
