//
//  ViewController.m
//  Fotomultas
//
//  Created by Marlon David Ruiz Arroyave on 3/09/15.
//  Copyright (c) 2015 Marlon David Ruiz Arroyave. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    usernameLabel.text = @"";
    passwordLabel.text = @"";
}

- (BOOL)shouldPerformSegueWithIdentifier:(NSString *)identifier sender:(id)sender{
    
    [self.view endEditing:YES];
    BOOL should = [usernameLabel.text isEqualToString:@"admin"] && [passwordLabel.text isEqual:@"1234"];
    
    if (!should) {
        UIAlertView *alertview = [[UIAlertView alloc]initWithTitle:@"Login" message:@"Error with credentials. Try again." delegate:self cancelButtonTitle:@"Cancel" otherButtonTitles:@"OK", nil];
        
        [alertview show];
    }
    
    return should;
    
}

#pragma mark - UIAlertViewDelegate

- (void)alertView:(UIAlertView *)alertView clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex == 1) {
        usernameLabel.text = @"";
        passwordLabel.text = @"";
    }
}



@end
