//
//  Multa.h
//  Fotomultas
//
//  Created by Marlon David Ruiz Arroyave on 4/09/15.
//  Copyright (c) 2015 Marlon David Ruiz Arroyave. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Multa : NSObject

@property (strong, nonatomic) NSString *fecha;
@property (strong, nonatomic) NSString *infraccion;
@property (strong, nonatomic) NSString *sancion;
@property (strong, nonatomic) NSString *imageMulta;

- (instancetype)initWithFecha:(NSString *)fecha
                   infraccion:(NSString *)infraccion
                      sancion:(NSString *)sancion
                   imageMulta:(NSString *)imageMulta;

- (instancetype)initWithDictionary:(NSDictionary *)dictionary;

+ (NSArray *)multas;
+ (NSArray *)fotoMultas;

@end
