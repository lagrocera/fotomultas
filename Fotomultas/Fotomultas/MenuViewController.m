//
//  MenuViewController.m
//  Fotomultas
//
//  Created by Marlon David Ruiz Arroyave on 3/09/15.
//  Copyright (c) 2015 Marlon David Ruiz Arroyave. All rights reserved.
//

#import "MenuViewController.h"
#import "ListViewController.h"
#import "Multa.h"

@interface MenuViewController ()

@end

@implementation MenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    ListViewController *listVC = [segue destinationViewController];
    if ([segue.identifier isEqualToString:@"Fotomultas"]) {

        listVC.type  = @"FotomultasCell";
        listVC.multas = [Multa fotoMultas];
        
    }else{
        
        listVC.type  = @"MultasCell";
        listVC.multas = [Multa multas];
    }
    
}


- (IBAction)logout:(id)sender {
    
    [self dismissViewControllerAnimated:YES completion:nil];
    
}
@end
