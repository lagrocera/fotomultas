//
//  DetailViewController.m
//  Fotomultas
//
//  Created by Marlon David Ruiz Arroyave on 3/09/15.
//  Copyright (c) 2015 Marlon David Ruiz Arroyave. All rights reserved.
//

#import "DetailViewController.h"

@interface DetailViewController ()

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.fechaLabel.text = self.multa.fecha;
    self.infraccionLabel.text = self.multa.infraccion;
    self.imageMulta.image = [UIImage imageNamed:self.multa.imageMulta];
    self.sancionLabel.text = self.multa.sancion;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
