//
//  Multa.m
//  Fotomultas
//
//  Created by Marlon David Ruiz Arroyave on 4/09/15.
//  Copyright (c) 2015 Marlon David Ruiz Arroyave. All rights reserved.
//

#import "Multa.h"

@implementation Multa

- (instancetype)initWithFecha:(NSString *)fecha
                   infraccion:(NSString *)infraccion
                      sancion:(NSString *)sancion
                   imageMulta:(NSString *)imageMulta{
    
    if (self = [super init]) {
    
        self.fecha = fecha;
        self.infraccion = infraccion;
        self.sancion = sancion;
        self.imageMulta = (imageMulta)?imageMulta:@"";
    }
    
    return self;
    
}

- (instancetype)initWithDictionary:(NSDictionary *)dictionary{
    
    if (self = [super init]) {
        
        self.fecha = dictionary[@"fecha"];
        self.infraccion = dictionary[@"infraccion"];
        self.sancion = dictionary[@"sancion" ];
        self.imageMulta = (dictionary[@"imageMulta" ])?dictionary[@"imageMulta" ]:@"";
    }
    
    return self;

    
}

+ (NSArray *)multas{
    
    Multa *multa = [[Multa alloc]initWithFecha:@"15.01.2010"
                                    infraccion:@"Semaforo en rojo"
                                       sancion:@"260000" imageMulta:nil];
    Multa *multa2 = [[Multa alloc]initWithFecha:@"01.01.2010"
                                    infraccion:@"Semaforo en rojo"
                                       sancion:@"260000" imageMulta:nil];
    Multa *multa3 = [[Multa alloc]initWithFecha:@"30.01.2010"
                                    infraccion:@"Mal parqueado"
                                       sancion:@"280000" imageMulta:nil];
    
    return @[multa,multa2,multa3];
}

+ (NSArray *)fotoMultas{
    
    NSMutableArray * multas = [NSMutableArray array];
    
    NSArray *array = @[
                       
                       @{
                           @"fecha": @"23.05.2014",
                           @"infraccion":@"semaforo en rojo",
                           @"sancion":@"260000",
                           @"imageMulta":@"fotomulta.jpg"
                           },
                       
                       @{
                           @"fecha": @"23.05.2000",
                           @"infraccion":@"semaforo en rojo",
                           @"sancion":@"560000",
                           @"imageMulta":@"fotomulta.jpg"
                           },
                       @{
                           @"fecha": @"02.05.2013",
                           @"infraccion":@"semaforo en rojo",
                           @"sancion":@"280000",
                           @"imageMulta":@"fotomulta.jpg"
                           }
                       
                       ];
    
    for (NSDictionary *dict in array) {
        Multa *multa = [[Multa alloc]initWithDictionary:dict];
        [multas addObject:multa];
    }
    
    return multas.copy;
}

@end
