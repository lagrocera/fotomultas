//
//  DetailViewController.h
//  Fotomultas
//
//  Created by Marlon David Ruiz Arroyave on 3/09/15.
//  Copyright (c) 2015 Marlon David Ruiz Arroyave. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "Multa.h"

@interface DetailViewController : UIViewController

@property (strong, nonatomic) IBOutlet UIImageView *imageMulta;
@property (strong, nonatomic) IBOutlet UILabel *fechaLabel;
@property (strong, nonatomic) IBOutlet UILabel *infraccionLabel;

@property (strong, nonatomic) IBOutlet UILabel *sancionLabel;
@property (strong, nonatomic) IBOutlet UIView *viewDetailSancion;

@property (strong, nonatomic) Multa *multa;

@end
