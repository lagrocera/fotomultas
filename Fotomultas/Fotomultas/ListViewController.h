//
//  ListViewController.h
//  Fotomultas
//
//  Created by Marlon David Ruiz Arroyave on 3/09/15.
//  Copyright (c) 2015 Marlon David Ruiz Arroyave. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListViewController : UIViewController

@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (strong, nonatomic) NSString *type;
@property (strong, nonatomic) NSArray *multas;

@end
