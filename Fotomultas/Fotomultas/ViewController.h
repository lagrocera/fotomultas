//
//  ViewController.h
//  Fotomultas
//
//  Created by Marlon David Ruiz Arroyave on 3/09/15.
//  Copyright (c) 2015 Marlon David Ruiz Arroyave. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{
    
    IBOutlet UITextField *usernameLabel;
    IBOutlet UITextField *passwordLabel;
    
}


@end

