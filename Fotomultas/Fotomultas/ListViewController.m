//
//  ListViewController.m
//  Fotomultas
//
//  Created by Marlon David Ruiz Arroyave on 3/09/15.
//  Copyright (c) 2015 Marlon David Ruiz Arroyave. All rights reserved.
//

#import "ListViewController.h"
#import "MultaCell.h"
#import "Multa.h"
#import "DetailViewController.h"

@interface ListViewController ()

@end

@implementation ListViewController{
    
  
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
    
    NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
    Multa *multa = self.multas[indexPath.row];
    
    DetailViewController *detailVC = [segue destinationViewController];
    detailVC.multa = multa;
    
}


#pragma mark - UITableViewDataSource
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{

    return self.multas.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    MultaCell *cell = (MultaCell *) [tableView dequeueReusableCellWithIdentifier:self.type];
    Multa *multa = self.multas[indexPath.row];
    
    cell.infraccionLabel.text = multa.infraccion;
    cell.fechaLabel.text = multa.fecha;
    
    if ([self.type isEqualToString:@"FotomultasCell"]) {
        cell.imageMulta.image = [UIImage imageNamed:multa.imageMulta];
        
    }
    
    return cell;
}

#pragma mark - UITableViewDelegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    [self performSegueWithIdentifier:@"DetalleMulta" sender:self.view];

}


@end
